CREATE TABLE IF NOT EXISTS logs (
    id SERIAL PRIMARY KEY,
    log_time TIMESTAMP NOT NULL DEFAULT current_timestamp,
    log_level VARCHAR(10) NOT NULL,
    message TEXT NOT NULL
);

-- Добавление индекса для улучшения производительности запросов на поиск по времени и уровню журнала
CREATE INDEX IF NOT EXISTS idx_log_time ON logs (log_time);
CREATE INDEX IF NOT EXISTS idx_log_level ON logs (log_level);
