FROM apache/airflow:2.2.2  # Используйте актуальную версию

WORKDIR /opt/airflow

USER root 

RUN apt update && \
    apt install -y procps default-jre && \
    apt clean

USER airflow 

COPY ./dags/* ./dags/
COPY ./spark_jobs/* ./spark_jobs/
COPY ./data/* ./data/

RUN pip install apache-airflow-providers-apache-spark plyvel

EXPOSE 8080
